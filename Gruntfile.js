module.exports = function(grunt) {

  var themePath = 'wp-content/themes/mba/';
  var srcPath = themePath + 'src/';
  var buildPath = themePath + 'build/';

  grunt.initConfig({

    pkg: grunt.file.readJSON('package.json'),

    autoprefixer: {
      options: {
        browsers: ['> 1%']
      },
      build: {
        files: [{
          expand: true,
          src: srcPath + 'style/*.css',
          dest: ''
        }]
      }
    },
    concat: {
      vendorJS: {
        src: [
          srcPath + 'scripts/vendor/jquery/*.js',
          srcPath + 'scripts/vendor/*.js'
        ],
        dest: buildPath + 'scripts/vendor.js'
      },
      fallbackJS: {
        src: srcPath + 'scripts/fallback/*.js',
        dest: buildPath + 'scripts/fallback.js'
      },
      vendorCSS: {
        src: srcPath + 'style/vendor/*.css',
        dest: buildPath + 'style/vendor.css'
      },
      buildJS: {
        src: srcPath + 'scripts/*.js',
        dest: buildPath + 'scripts/scripts.js'
      }
    },
    copy: {
      vendor: {
        files: [
          {
            expand: true,
            flatten: true,
            src: [
              'bower_components/bootstrap-sass-official/assets/stylesheets/*'
            ],
            dest: srcPath + 'style/bootstrap/'
          },
          {
            expand: true,
            flatten: true,
            src: [
              'bower_components/bootstrap-sass-official/assets/stylesheets/bootstrap/*'
            ],
            dest: srcPath + 'style/bootstrap/bootstrap/'
          },
          {
            expand: true,
            flatten: true,
            src: [
              'bower_components/bootstrap-sass-official/assets/stylesheets/bootstrap/mixins/*'
            ],
            dest: srcPath + 'style/bootstrap/bootstrap/mixins/'
          },
          {
            expand: true,
            flatten: true,
            src: [
              'bower_components/fontawesome/scss/*'
            ],
            dest: srcPath + 'style/fontawesome/'
          },
          {
            expand: true,
            flatten: true,
            src: [
              'bower_components/jquery-ui/jquery-ui.js',
              'bower_components/bootstrap-sass-official/assets/javascripts/bootstrap.js'
            ],
            dest: srcPath + 'scripts/vendor/'
          },
          {
            expand: true,
            flatten: true,
            src: [
              'bower_components/bootstrap-sass-official/assets/fonts/bootstrap/*',
              'bower_components/fontawesome/fonts/*'
            ],
            dest: buildPath + 'fonts/'
          }
        ]
      },
      fallback: {
        files: [
          {
            expand: true,
            flatten: true,
            src: [
              'bower_components/respond/dest/respond.min.js',
              'bower_components/html5shiv/dist/html5shiv.min.js',
              'bower_components/REM-unit-polyfill/js/rem.js'
            ],
            dest: srcPath + 'scripts/fallback/'
          },
        ]
      }
    },
    imagemin: {
      dynamic: {
        files: [{
          expand: true,
          cwd: 'images/',
          src: ['**/*.{png,jpg,gif}'],
          dest: 'images/'
        }]
      }
    },
    jshint: {
      build: ['Gruntfile.js', srcPath + 'scripts/*.js']
    },
    sass: {
      build: {
        options: {
          style: 'compressed'
        },
        files: [{
          expand: true,
          cwd: srcPath + 'style',
          src: ['*.scss'],
          dest: buildPath + 'style/',
          ext: '.min.css'
        }]
      }
    },
    uglify: {
      vendor: {
        files: [{
          expand: true,
          cwd: buildPath + 'scripts/',
          src: ['vendor.js'],
          dest: buildPath + 'scripts/',
          ext: '.min.js'
        }]
      },
      fallback: {
        files: [{
          expand: true,
          cwd: buildPath + 'scripts/',
          src: ['fallback.js'],
          dest: buildPath + 'scripts/',
          ext: '.min.js'
        }]
      },
      build: {
        files: [{
          expand: true,
          cwd: buildPath + 'scripts/',
          src: ['scripts.js'],
          dest: buildPath + 'scripts/',
          ext: '.min.js'
        }]
      }
    },
    watch: {
      scripts: {
        files: [srcPath + 'scripts/**/*.js'],
        tasks: ['jshint', 'concat:buildJS', 'uglify:build']
      },
      style: {
        files: [srcPath + 'style/**/*.scss'],
        tasks: ['sass', 'autoprefixer']
      },
      images: {
        files: ['images/**/*.{png,jpg,gif}'],
        tasks: ['imagemin'],
        options: {
          spawn: false
        }
      }
    }
  });

  grunt.loadNpmTasks('grunt-autoprefixer');
  grunt.loadNpmTasks('grunt-contrib-concat');
  grunt.loadNpmTasks('grunt-contrib-copy');
  grunt.loadNpmTasks('grunt-contrib-imagemin');
  grunt.loadNpmTasks('grunt-contrib-jshint');
  grunt.loadNpmTasks('grunt-contrib-sass');
  grunt.loadNpmTasks('grunt-contrib-uglify');
  grunt.loadNpmTasks('grunt-contrib-watch');

  grunt.registerTask('build', ['sass', 'autoprefixer', 'jshint', 'concat', 'uglify', 'imagemin']);
  grunt.registerTask('dev', ['build', 'watch']);
  grunt.registerTask('default', ['copy', 'build']);

};
