<?php
/*
Plugin Name: MBA People
Description: A plugin to manage people within an organization
Version: 1.0
Author: Robert Sefer
Author URI: http://rsefer.com
*/

function create_people_posttype() {
  register_post_type('person', array(
    'labels' => array(
      'name' => __('People'),
      'singular_name' => __('Person'),
      'add_new' => 'Add New Person',
      'edit_item' => 'Edit Person',
      'view_item' => 'View Person',
      'search_items' => 'Search People',
      'not_found' => 'No people found',
      'not_found_in_trash' => 'No people found in Trash'
    ),
    'description' => 'People in the organization.',
    'public' => true,
    'has_archive' => true,
    'rewrite' => array('slug' => 'people'),
    'menu_icon' => 'dashicons-businessman',
    'supports' => array('title','editor','revisions','thumbnail','page-attributes'),
    'register_meta_box_cb' => 'add_people_metaboxes'
  ));
}
add_action('init', 'create_people_posttype');

function get_custom_post_type_archive_template($template) {
  global $post;
  if (is_post_type_archive('person')) {
    $template = dirname(__FILE__) . '/templates/archive-person.php';
  }
  return $template;
}
add_filter('archive_template', 'get_custom_post_type_archive_template');

function get_custom_post_type_single_template($template) {
  global $post;
  if (is_singular('person')) {
    $template = dirname(__FILE__) . '/templates/single-person.php';
  }
  return $template;
}
add_filter('single_template', 'get_custom_post_type_single_template');

function add_people_metaboxes() {
  add_meta_box('people_information', 'Personal Information', 'people_information', 'person', 'normal', 'high');
}
add_action('add_meta_boxes', 'add_people_metaboxes');

function people_information() {
  global $post;
  echo '<input type="hidden" name="personmeta_noncename" id="personmeta_noncename" value="' . wp_create_nonce( plugin_basename(__FILE__) ) . '" />';
  $jobtitle = get_post_meta($post->ID, '_jobtitle', true);
  echo '<p><label for="personmeta_jobtitle">Job Title:</label></p>';
  echo '<input type="text" name="_jobtitle" id="personmeta_jobtitle" value="' . $jobtitle  . '" class="widefat" />';
  $phone = get_post_meta($post->ID, '_phone', true);
  echo '<p><label for="personmeta_phone">Phone: (10 digits only)</label></p>';
  echo '<input type="text" name="_phone" id="personmeta_phone" value="' . $phone  . '" class="widefat" />';
  $email = get_post_meta($post->ID, '_email', true);
  echo '<p><label for="personmeta_email">Email:</label></p>';
  echo '<input type="text" name="_email" id="personmeta_email" value="' . $email  . '" class="widefat" />';
  $nickname = get_post_meta($post->ID, '_nickname', true);
  echo '<p><label for="personmeta_nickname">Short name/nickname: (Ex. "Bob")</label></p>';
  echo '<input type="text" name="_nickname" id="personmeta_nickname" value="' . $nickname  . '" class="widefat" />';
}

function save_person_meta($post_id, $post) {
  if (!wp_verify_nonce( $_POST['personmeta_noncename'], plugin_basename(__FILE__))) {
    return $post->ID;
  }
  if (!current_user_can( 'edit_post', $post->ID )) {
    return $post->ID;
  }
  $person_meta['_jobtitle'] = $_POST['_jobtitle'];
  $person_meta['_phone'] = $_POST['_phone'];
  $person_meta['_email'] = $_POST['_email'];
  $person_meta['_nickname'] = $_POST['_nickname'];
  foreach ($person_meta as $key => $value) {
    if ($post->post_type == 'revision') return;
    $value = implode(',', (array)$value);
    if(get_post_meta($post->ID, $key, FALSE)) {
      update_post_meta($post->ID, $key, $value);
    } else {
      add_post_meta($post->ID, $key, $value);
    }
    if (!$value) delete_post_meta($post->ID, $key);
  }

}
add_action('save_post', 'save_person_meta', 1, 2);

?>
