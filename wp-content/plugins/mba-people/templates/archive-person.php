<?php get_header(); ?>

<section id="people-intro" class="hero pattern">
  <div class="container">
    <div class="row">
      <div class="col-sm-12">
        <h1>Our People</h1>
      </div>
    </div>
  </div>
</section>

<section id="people-main">
  <div class="container">
    <div class="row">
      <?php if(have_posts()) : while(have_posts()) : the_post(); ?>
      <div class="col-sm-6" itemscope itemtype="http://schema.org/Person">
        <?php
          $person_jobtitle = get_post_meta(get_the_ID(), '_jobtitle', true);
          $person_nickname = get_post_meta(get_the_ID(), '_nickname', true);
          if ($person_nickname) {
            $working_nickname = $person_nickname;
          } else {
            $working_nickname = substr(get_the_title(), 0, strpos(get_the_title(), ' '));
          }
        ?>
        <div class="row">
          <div class="col-sm-6">
            <?php the_post_thumbnail(); ?>
          </div>
          <div class="col-sm-6">
            <h3 itemprop="name"><?php the_title(); ?></h3>
            <?php
            if ($person_jobtitle) {
              echo '<h4 itemprop="jobTitle">' . $person_jobtitle . '</h4>';
            }
            ?>
            <p><a href="<?php echo get_the_permalink(get_the_ID()); ?>">Read more about <?php echo $working_nickname; ?></a></p>
          </div>
        </div>
      </div>
      <?php endwhile; endif; ?>
      <?php get_template_part('includes/contact-block'); ?>
    </div>
  </div>
</section>

<?php get_footer(); ?>
