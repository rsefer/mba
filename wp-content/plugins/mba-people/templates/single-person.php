<?php get_header(); ?>

<?php
  $person_jobtitle = get_post_meta(get_the_ID(), '_jobtitle', true);
  $person_phone = get_post_meta(get_the_ID(), '_phone', true);
  $person_email = get_post_meta(get_the_ID(), '_email', true);
  $person_nickname = get_post_meta(get_the_ID(), '_nickname', true);
  if ($person_nickname) {
    $working_nickname = $person_nickname;
  } else {
    $working_nickname = substr(get_the_title(), 0, strpos(get_the_title(), ' '));
  }
?>

<div itemscope itemtype="http://schema.org/Person">

<section id="person-intro" class="hero person pattern">
  <div class="container">
    <div class="row">
      <?php if (has_post_thumbnail()) { ?>
      <div class="col-sm-5 headshot-wrap">
        <?php the_post_thumbnail(); ?>
      </div>
      <?php } ?>
      <div class="col-sm-7">
        <h1 class="person-name" itemprop="name"><?php the_title(); ?></h1>
        <?php
          if ($person_jobtitle) {
            echo '<h3 class="person-jobtitle" itemprop="jobTitle">' . $person_jobtitle . '</h3>';
          }
          if ($person_phone) {
            echo '<h5 class="person-phone"><i class="fa fa-fw fa-phone left yellow"></i><a href="tel:' . $person_phone . '" itemprop="telephone">' . phoneNumberConversion($person_phone) . '</a></h5>';
          }
          if ($person_email) {
            echo '<h5 class="person-email"><i class="fa fa-fw fa-envelope left green"></i><a href="mailto:' . $person_email . '" target="_blank">Email ' . $working_nickname . '</a><span class="hidden" itemprop="email">' . $person_email . '</span></h5>';
          }
        ?>
      </div>
    </div>
  </div>
</section>

<section id="person-main">
  <div class="container">
    <div class="row">
      <div class="col-sm-12">
        <?php if(have_posts()) : while(have_posts()) : the_post(); ?>
        <article id="post-<?php the_ID(); ?>" <?php post_class(); ?> itemprop="description">
          <?php the_content(); ?>
        </article>
        <?php endwhile; endif; ?>
        <?php get_template_part('includes/contact-block'); ?>
      </div>
    </div>
  </div>
</section>

</div>

<?php get_footer(); ?>
