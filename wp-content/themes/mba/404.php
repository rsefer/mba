<?php get_header(); ?>

<section id="error404-intro" class="hero">
  <div class="container">
    <div class="col-sm-12">
      <h1>Page Not Found</h1>
    </div>
  </div>
</section>

<section id="error404-main" class="main">
  <div class="container">
    <div class="col-sm-12">
      <article id="post-error404">
        <div class="article-content">
          <p>The page you are trying to reach cannot be found. <a href="<?php echo get_the_permalink(12); ?>">Contact us</a> or try one of the following pages:</p>
          <ul class="pages-list">
            <?php wp_list_pages(array(
              'title_li' => null
            )); ?>
          </ul>
        </div>
      </article>
    </div>
  </div>
</section>

<?php get_footer(); ?>
