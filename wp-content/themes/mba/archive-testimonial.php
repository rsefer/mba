<?php get_header(); ?>

<section id="testiominals-intro" class="hero pattern">
  <div class="container">
    <div class="row">
      <div class="col-sm-12">
        <h1>Testimonials</h1>
      </div>
    </div>
  </div>
</section>

<section id="testimonial-main">
  <div class="container">
    <div class="row">
      <?php if(have_posts()) : while(have_posts()) : the_post(); ?>
      <div class="col-sm-12 testimonial">
        <?php
          $testimonial_attribution = get_field('attribution');
          $testimonial_attributor_note = get_field('attributor_note');
        ?>
        <div class="testimonial-quote">
          <?php the_content(); ?>
        </div>
        <?php if ($testimonial_attribution) { ?>
        <div class="testimonial-attribution">
          &mdash; <?php echo $testimonial_attribution; ?><?php
            if ($testimonial_attributor_note) {
              echo ', <span class="attributor-note">' . $testimonial_attributor_note . '</span>';
            }
          ?>
        </div>
        <?php } ?>
      </div>
      <?php endwhile; endif; ?>
      <?php get_template_part('includes/contact-block'); ?>
    </div>
  </div>
</section>

<?php get_footer(); ?>
