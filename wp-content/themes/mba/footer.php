<footer>
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <nav class="footer-nav">
          <?php
            wp_nav_menu(array(
              'menu' => 'main-menu',
              'depth' => 2
            ));
          ?>
        </nav>
      </div>
      <?php echo allAddresses(); ?>
    </div>
    <div class="fine-print row">
      <div class="col-sm-12">
        <?php echo wpautop(get_option('company_info_footer_fineprint')); ?>
      </div>
    </div>
  </div>
</footer>

<?php wp_footer(); ?>

<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/build/scripts/vendor.min.js?v=2"></script>
<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/build/scripts/scripts.min.js?v=4"></script>

<script>
(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
})(window,document,'script','//www.google-analytics.com/analytics.js','ga');

ga('create', 'UA-57314009-1', 'auto');
ga('send', 'pageview');

</script>

<!-- Google Code for Remarketing Tag -->
<script type="text/javascript">
/* <![CDATA[ */
var google_conversion_id = 955209468;
var google_custom_params = window.google_tag_params;
var google_remarketing_only = true;
/* ]]> */
</script>
<script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
</script>
<noscript>
<div style="display:inline;">
<img height="1" width="1" style="border-style:none;" alt="" src="//googleads.g.doubleclick.net/pagead/viewthroughconversion/955209468/?value=0&amp;guid=ON&amp;script=0"/>
</div>
</noscript>

</body>
</html>
