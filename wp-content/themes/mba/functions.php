<?php

# Hides Admin Bar
function hide_admin_bar() { return false; }
add_filter('show_admin_bar', 'hide_admin_bar');

# Menus
function register_my_menus() {
  register_nav_menu('main-menu', __('Main Menu'));
  register_nav_menu('about-menu', __('About Menu'));
}
add_action('init', 'register_my_menus');

# Post Thumbnails
add_theme_support('post-thumbnails');

# Content Width
if (!isset($content_width)) { $content_width = 960; }

# Remove unneeded menu items
function remove_wp_menu_items() {
  remove_menu_page('edit-comments.php');
}
add_action('admin_menu', 'remove_wp_menu_items');

# WP Nav Walker
require_once('includes/wp_bootstrap_navwalker.php');

# Bootstrap new line;
function bsNewLine() {
  return '<br class="hidden-xs"><span class="hidden-sm hidden-md hidden-lg">&nbsp;</span>';
}

# Shortcodes

function shortcode_emailContactLink() {
  return '<a class="contact-email-link" href="mailto:' . get_option('company_info_email') . '" target="_blank"><i class="fa fa-envelope left"></i>' . get_option('company_info_email') . '</a>';
}
add_shortcode('emailContactLink', 'shortcode_emailContactLink');

function shortcode_phoneContactLink($atts) {
  if ((isset($atts['vanity']) && $atts['vanity'] == 'hide') || empty(get_option('company_info_phone_vanity'))) {
    $phoneMarkup = '<a class="contact-phone-link" href="tel:' . get_option('company_info_phone') . '" target="_blank"><i class="fa fa-phone left"></i>' . phoneNumberConversion(get_option('company_info_phone')) . '</a>';
  } else {
    $phoneMarkup = '<a class="contact-phone-link" href="tel:' . get_option('company_info_phone') . '" target="_blank"><i class="fa fa-phone left"></i>' . get_option('company_info_phone_vanity') . '</a> or <a class="contact-phone-link" href="tel:' . get_option('company_info_phone') . '" target="_blank"><i class="fa fa-phone left"></i>' . phoneNumberConversion(get_option('company_info_phone')) . '</a>';
  }
  return $phoneMarkup;
}
add_shortcode('phoneContactLink', 'shortcode_phoneContactLink');

# Scripts
function mba_scripts() {
  wp_enqueue_script('jquery');
}
add_action('wp_enqueue_scripts', 'mba_scripts');

# Company Info Panel
$themename = 'Company Info';
$shortname = 'company_info';

$options = array(
  array(
    'name' => $themename,
    'type' => 'title'),
  array(
    'name' => 'Address / Contact Information',
    'type' => 'section'),
  array('type' => 'open'),
  array(
    'name' => 'Address 1',
    'id' => $shortname.'_address_1',
    'type' => 'text',
    'std' => ''),
  array(
    'name' => 'Address 2',
    'id' => $shortname.'_address_2',
    'type' => 'text',
    'std' => ''),
  array(
    'name' => 'City',
    'id' => $shortname.'_city',
    'type' => 'text',
    'std' => ''),
  array(
    'name' => 'State (Abbrev.)',
    'id' => $shortname.'_state',
    'type' => 'text',
    'std' => ''),
  array(
    'name' => 'Zipcode',
    'id' => $shortname.'_zipcode',
    'type' => 'text',
    'std' => ''),
  array(
    'name' => 'Phone # (10 digits only)',
    'id' => $shortname.'_phone',
    'type' => 'text',
    'std' => ''),
  array(
    'name' => 'Phone # Vanity (if applicable)',
    'id' => $shortname.'_phone_vanity',
    'type' => 'text',
    'std' => ''),
  array(
    'name' => 'Fax # (10 digits only)',
    'id' => $shortname.'_fax',
    'type' => 'text',
    'std' => ''),
    array(
      'name' => 'Contact Email Address',
      'id' => $shortname.'_email',
      'type' => 'text',
      'std' => ''),
  array('type' => 'close'),
  array(
    'name' => 'Miscellaneous',
    'type' => 'section'),
  array('type' => 'open'),
  array(
    'name' => 'Footer Fine Print',
    'id' => $shortname.'_footer_fineprint',
    'type' => 'textarea',
    'std' => ''),
  array('type' => 'close'),
  array('type' => 'save')
);

function mytheme_add_admin() {
global $themename, $shortname, $options;
if ($_GET['page'] == basename(__FILE__)) {
if ('save' == $_REQUEST['action']) {
foreach ($options as $value) {
  update_option($value['id'], $_REQUEST[$value['id']]);
}

foreach ($options as $value) {
  if(isset($_REQUEST[$value['id']])) {
    update_option($value['id'], $_REQUEST[$value['id']]);
  } else {
    delete_option($value['id']);
  }
}

header('Location: admin.php?page=functions.php&saved=true');
die;
} else if('reset' == $_REQUEST['action']) {
foreach ($options as $value) {
  delete_option($value['id']);
}

header('Location: admin.php?page=functions.php&reset=true');
die;
}
}

add_menu_page($themename, $themename, 'administrator', basename(__FILE__), 'mytheme_admin');
}

function mytheme_add_init() {
$file_dir = get_bloginfo('template_directory');
wp_enqueue_style('functions', $file_dir.'/functions/functions.css', false, '1.0', 'all');
wp_enqueue_script('rm_script', $file_dir.'/functions/rm_script.js', false, '1.0');
}

function mytheme_admin() {
global $themename, $shortname, $options;
$i = 0;

if ($_REQUEST['saved']) echo '<div id="message" class="updated fade"><p><strong>'.$themename.' saved.</strong></p></div>';
if ($_REQUEST['reset']) echo '<div id="message" class="updated fade"><p><strong>'.$themename.' reset.</strong></p></div>';
?>
<style>
.rm_input input {
width: 100%;
}
</style>
<div class="wrap rm_wrap">
<h2><?php echo $themename; ?></h2>
<p><small>Developer note: output each setting with '<b>echo stripslashes(get_option('<?php echo $shortname; ?>_[variable name]'))</b>'.</small></p>
<div class="rm_opts">
<form method="post">
  <?php foreach ($options as $value) {
    switch ($value['type']) {
      case "open":
      break;
      case "close":
      echo '</div></div><br /><hr>';
      break;
      case 'text':
      ?>
      <div class="rm_input rm_text">
        <label for="<?php echo $value['id']; ?>"><?php echo $value['name']; ?></label> <small><?php echo $value['desc']; ?></small><br>
        <input name="<?php echo $value['id']; ?>" id="<?php echo $value['id']; ?>" type="<?php echo $value['type']; ?>" value="<?php if (get_settings($value['id']) != "") { echo htmlspecialchars(stripslashes(get_settings($value['id']))); } else { echo htmlspecialchars($value['std']); } ?>" />

        <div class="clearfix"></div>
      </div>
      <?php
      break;
      case 'textarea':
      ?>
      <div class="rm_input rm_textarea">
      <label for="<?php echo $value['id']; ?>"><?php echo $value['name']; ?></label> <small><?php echo $value['desc']; ?></small><br>
      <?php
      if ( get_settings( $value['id'] ) != "") {
        $initialc =  stripslashes(get_settings( $value['id']) );
      }
      ?>
      <?php wp_editor($initialc, $value['id'], array(
        'media_buttons' => false,
        'teeny' => true
      )); ?>
      <div class="clearfix"></div>
    </div>
    <?php
    break;
      case 'checkbox':
      ?>
      <div class="rm_input rm_checkbox">
        <?php if(get_option($value['id'])){ $checked = "checked=\"checked\""; }else{ $checked = "";} ?>
        <input style="width: auto;" type="checkbox" name="<?php echo $value['id']; ?>" id="<?php echo $value['id']; ?>" value="true" <?php echo $checked; ?> />
        <label for="<?php echo $value['id']; ?>"><?php echo $value['name']; ?></label>
        <br><small><?php echo $value['desc']; ?></small>
        <div class="clearfix"></div>
      </div>
      <?php
      break;
      case 'section':
      $i++;
      ?>
      <div class="rm_section">
        <div class="rm_title">
          <h3><?php echo $value['name']; ?></h3>
          <!--<span class="submit"><input name="save<?php echo $i; ?>" type="submit" value="Save Changes" class="button button-primary button-large" /></span>-->
          <div class="clearfix"></div>
        </div>
        <div class="rm_options">
          <?php
          break;
          case 'save';
          ?>
          <span class="submit"><input name="save<?php echo $i; ?>" type="submit" value="Save Changes" class="button button-primary button-large" /></span>
          <?php
          break;
        }
      }
      ?>
      <input type="hidden" name="action" value="save" />
    </form>
  </div>
  <?php

}

add_action('admin_init', 'mytheme_add_init');
add_action('admin_menu', 'mytheme_add_admin');

# Phone Number Conversion
function phoneNumberConversion($number) {
  if (strlen($number) == 10) {
    $result = '(' . substr($number, 0, 3) . ') ' . substr($number, 3, 3) . '-' . substr($number, 6, 4);
    return $result;
  } else {
    return $number;
  }
}

# Company Address
function fullAddress($includeTitle = true, $includePhone = true, $includeFax = true) {
  $return = '<div itemscope itemtype="http://schema.org/LocalBusiness">';
    if ($includeTitle) { $return .= '<i class="fa fa-fw left"></i><span class="business-name" itemprop="name">' . get_bloginfo('name') . '</span>'; }
    $return .= '<div itemprop="address" itemscope itemtype="http://schema.org/PostalAddress">';
      $return .= '<i class="fa fa-fw fa-map-marker left green"></i><a href="' . addressGoogleMapsURL() . '"><span itemprop="streetAddress">' . get_option('company_info_address_1') . '</span>';
      if (get_option('company_info_address_2')) { $return .= ' ' . get_option('company_info_address_2'); }
      $return .= '<br><span itemprop="addressLocality"><i class="fa fa-fw left"></i>' . get_option('company_info_city') . '</span>, <span itemprop="addressRegion">' . get_option('company_info_state') . '</span> <span itemprop="postalCode">' . get_option('company_info_zipcode') . '</span>';
      $return .= '</a>';
    $return .= '</div>';
    if ($includePhone && get_option('company_info_phone')) { $return .= '<div><i class="fa fa-fw fa-phone left yellow"></i><a href="tel:' . get_option('company_info_phone') . '">Phone: <span itemprop="telephone">' .  phoneNumberConversion(get_option('company_info_phone')) . '</span></a></div>'; }
    if ($includeFax && get_option('company_info_fax')) { $return .= '<div><i class="fa fa-fw fa-fax left green"></i>Fax: <span itemprop="telephone">' . phoneNumberConversion(get_option('company_info_fax')) . '</span></div>'; }
  $return .= '</div>';

  return $return;
}

function allAddresses($bsClasses = 'col-lg-3 col-lg-offset-0 col-sm-6 col-sm-offset-0 ') {
  return '<div class="clearfix"></div><div class="' . $bsClasses . 'fullAddress">' . fullAddress() . '</div>
  <div class="' . $bsClasses . 'fullAddress">
    <div itemscope itemtype="http://schema.org/LocalBusiness"><i class="fa fa-fw left"></i><span class="business-name" itemprop="name">MBA, LLC Northside</span><div itemprop="address" itemscope itemtype="http://schema.org/PostalAddress"><i class="fa fa-fw fa-map-marker left yellow"></i><a href="https://www.google.com/maps/place/6200+N+Hiawatha+Ave,+Chicago,+IL+60646/"><span itemprop="streetAddress">6200 N. Hiawatha Ave.</span> Suite 400<br><span itemprop="addressLocality"><i class="fa fa-fw left"></i>Chicago</span>, <span itemprop="addressRegion">IL</span> <span itemprop="postalCode">60646</span></a></div></div>
  </div>
  <div class="clearfix visible-md visible-sm"></div>
  <div class="' . $bsClasses . 'fullAddress">
    <div itemscope itemtype="http://schema.org/LocalBusiness"><i class="fa fa-fw left"></i><span class="business-name" itemprop="name">MBA, LLC Southside</span><div itemprop="address" itemscope itemtype="http://schema.org/PostalAddress"><i class="fa fa-fw fa-map-marker left green"></i><a href="https://www.google.com/maps/place/10540+S+Western+Ave,+Chicago,+IL+60643/"><span itemprop="streetAddress">10540 S. Western Ave. Suite 309</span><br><span itemprop="addressLocality"><i class="fa fa-fw left"></i>Chicago</span>, <span itemprop="addressRegion">IL</span> <span itemprop="postalCode">60643</span></a></div></div>
  </div>
  <div class="' . $bsClasses . 'fullAddress">
    <div itemscope itemtype="http://schema.org/LocalBusiness"><i class="fa fa-fw left"></i><span class="business-name" itemprop="name">MBA, LLC Northbrook</span><div itemprop="address" itemscope itemtype="http://schema.org/PostalAddress"><i class="fa fa-fw fa-map-marker left yellow"></i><a href="https://www.google.com/maps/place/601+Skokie+Blvd,+Northbrook,+IL+60062/"><span itemprop="streetAddress">601 Skokie Blvd.</span> Suite 306<br><span itemprop="addressLocality"><i class="fa fa-fw left"></i>Northbrook</span>, <span itemprop="addressRegion">IL</span> <span itemprop="postalCode">60062</span></a></div></div>
  </div>';
}

function addressGoogleMapsURL() {
  $addressParts = get_option('company_info_address_1') . ', ' . get_option('company_info_city') . ', ' . get_option('company_info_state') . ' ' . get_option('company_info_zipcode');
  return 'https://www.google.com/maps/place/' . urlencode($addressParts) . '/';
}

function create_testimonials() {

  register_post_type('testimonial', array(
    'labels' => array(
      'name' => 'Testimonials',
      'singular_name' => 'Testimonials',
      'menu_name' => 'Testimonials',
      'name_admin_bar' => 'Testimonial',
      'add_new' => 'Add New',
      'add_new_item' => 'Add New Testimonial',
      'new_item' => 'New Testimonial',
      'edit_item' => 'Edit Testimonial',
      'view_item' => 'View Testimonial',
      'all_items' => 'All Testimonials',
      'search_items' => 'Search Testimonials',
      'not_found' => 'No testimonials found.',
      'not_found_in_trash' => 'No testimonial found in Trash.',
    ),
    'description' => 'Client testimonials.',
    'public' => true,
    'has_archive' => true,
    'menu_position' => 5,
    'menu_icon' => 'dashicons-editor-quote',
    'supports' => array('title', 'count', 'editor'),
    'rewrite' => array(
      'slug' => 'testimonials',
      'with_front' => false
    )
  ));

}
add_action('init', 'create_testimonials');

function wpsites_query($query) {
  if ($query->is_post_type_archive('testimonial') && $query->is_main_query() && !is_admin()) {
    $query->set('posts_per_page', 999);
  }
}
add_action('pre_get_posts', 'wpsites_query');

?>
