<?php include('includes/head.php'); ?>

<header>
  <nav class="header-nav navbar navbar-default navbar-fixed-top" role="navigation">
    <div class="container">
      <div class="navbar-header">
        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#main-navbar-collapse">
          <span class="sr-only">Toggle navigation</span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
        </button>
        <a class="navbar-brand" href="/"><img class="main-logo img-svg" src="/images/logo_white.svg" alt="Midwest Bankruptcy Attorneys LLC"><img class="main-logo img-ie" src="/images/logo_white.png" alt="Midwest Bankruptcy Attorneys LLC"></a>
      </div>
      <?php
        wp_nav_menu(array(
          'menu' => 'main-menu',
          'depth' => 3,
          'container' => 'div',
          'container_class' => 'collapse navbar-collapse',
          'container_id' => 'main-navbar-collapse',
          'menu_class' => 'nav navbar-nav navbar-right',
          'fallback_cb' => 'wp_bootstrap_navwalker::fallback',
          'walker' => new wp_bootstrap_navwalker()
        ));
      ?>
      <?php if (get_option('company_info_phone')) { ?><a class="nav-extra btn btn-warning" href="tel:<?php echo get_option('company_info_phone'); ?>">
        Call Now: <?php if (get_option('company_info_phone_vanity')) { ?><span class="phone-number primary"><?php echo get_option('company_info_phone_vanity'); ?></span><span class="hidden-sm hidden-xs"><br>or <span class="phone-number secondary"><?php echo phoneNumberConversion(get_option('company_info_phone')); ?></span></span><?php } else { ?></span><span class="phone-number big"><?php echo phoneNumberConversion(get_option('company_info_phone')); ?></span><?php } ?><span class="hidden-sm hidden-xs"><br>
        <div class="call-now-tag">For A Free Consultation</div></span>
      </a><?php } ?>
    </div>
  </nav>
</header>
