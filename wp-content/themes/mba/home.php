<?php get_header(); ?>

<section id="default-intro" class="hero<?php if (!has_post_thumbnail()) { echo ' pattern'; } ?>"<?php if (has_post_thumbnail()) { echo ' style="background-image: url(' . wp_get_attachment_url(get_post_thumbnail_id(get_the_ID())) . ');"'; } ?>>
  <div class="container">
    <div class="row">
      <div class="col-sm-12">
        <h1>Blog</h1>
      </div>
    </div>
  </div>
</section>

<section id="default-main" class="main">
  <div class="container">
    <div class="row">
      <div class="col-sm-12">
        <ul class="categories-list">
          <?php
            $categories = get_categories();
            foreach ($categories as $category) {
              echo '<li><a href="' . get_category_link($category->term_id) . '">' . $category->name . '</a></li>';
            }
          ?>
        </ul>
      </div>
      <div class="col-sm-12">
        <?php if(have_posts()) : while(have_posts()) : the_post(); ?>
        <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
          <h2><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
          <p><strong>Posted <?php the_date(); ?></strong></p>
          <div class="article-content">
            <?php the_excerpt(); ?>
            <p><a href="<?php the_permalink(); ?>"><strong>Read This Post<i class="fa fa-arrow-right right"></i></strong></a></p>
          </div>
        </article>
        <?php endwhile; else : ?>
        <article>
          <h1 class="title">Not Found</h1>
          <div class="article-content">
            <p class="lead text-center">Go to the <a href="/">home page</a>.</p>
          </div>
        </article>
        <?php endif; ?>
        <div class="prev-next-wrap">
          <div class="prev-next-item nav-previous pull-right"><?php next_posts_link('Older Posts<i class="fa fa-arrow-right right"></i>'); ?></div>
          <div class="prev-next-item nav-next pull-left"><?php previous_posts_link('<i class="fa fa-arrow-left left"></i>Newer Posts'); ?></div>
        </div>
        <?php get_template_part('includes/contact-block'); ?>
      </div>
    </div>
  </div>
</section>

<?php get_footer(); ?>
