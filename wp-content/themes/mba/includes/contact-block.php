<div class="contact-block">
  <div class="contact-title">Interested in talking?<span class="hidden-xs">&nbsp;</span><br class="visible-xs">Get in Touch</div>
  <div class="contact-content"><a class="contact-form-link" href="<?php echo get_the_permalink(12); ?>">Send us a note</a><span class="hidden-xs">, </span><br class="visible-xs">email us at<span class="hidden-xs">&nbsp;</span><br class="visible-xs"><?php echo do_shortcode('[emailContactLink]'); ?><span class="hidden-xs">, </span><br class="visible-xs">or call us at<span class="hidden-xs">&nbsp;</span><br class="visible-xs"><?php echo do_shortcode('[phoneContactLink]'); ?></div>
</div>
