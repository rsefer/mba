<div class="contact-links">
  <div class="contact-links-email">Email us: <?php echo do_shortcode('[emailContactLink]'); ?></div>
  <div class="contact-links-phone">Call us: <?php echo do_shortcode('[phoneContactLink]'); ?></div>
</div>
