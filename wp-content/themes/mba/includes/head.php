<!DOCTYPE html>
<html>
<head>
  <title><?php wp_title('|', true, 'right'); ?></title>

  <meta name="viewport" content="width=device-width,minimum-scale=1.0,initial-scale=1.0" />

  <link rel="apple-touch-icon-precomposed" href="/images/favicon_800.png">
  <link rel="shortcut icon" href="/images/favicon_192.png">

  <link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/build/style/style.min.css?v=13" />

  <!--[if lte IE 8]>
  <link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/build/style/fallback.min.css" />
  <script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/build/scripts/fallback.min.js"></script>
  <![endif]-->

  <script src="//use.typekit.net/seo5sjf.js"></script>
  <script>try{Typekit.load();}catch(e){}</script>

  <?php wp_head(); ?>
</head>
<body <?php body_class($class); ?>>
