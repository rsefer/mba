<?php get_header(); ?>

<section id="default-intro" class="hero<?php if (!has_post_thumbnail()) { echo ' pattern'; } ?>"<?php if (has_post_thumbnail()) { echo ' style="background-image: url(' . wp_get_attachment_url(get_post_thumbnail_id(get_the_ID())) . ');"'; } ?>>
  <div class="container">
    <div class="row">
      <div class="col-sm-12">
        <h1><?php the_title(); ?></h1>
      </div>
    </div>
  </div>
</section>

<section id="default-main" class="main">
  <div class="container">
    <div class="row">
      <?php if (is_single()) { ?>
      <div class="col-sm-12">
        <ul class="categories-list">
          <?php
            $categories = wp_get_post_categories(get_the_ID());
            foreach ($categories as $category) {
              echo '<li><a href="' . get_category_link($category) . '">' . get_category($category)->name . '</a></li>';
            }
          ?>
        </ul>
      </div>
      <?php } ?>
      <div class="col-sm-12">
        <?php if(have_posts()) : while(have_posts()) : the_post(); ?>
        <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
          <div class="article-content">
            <?php the_content(); ?>
          </div>
        </article>
        <?php endwhile; else : ?>
        <article>
          <h1 class="title">Not Found</h1>
          <div class="article-content">
            <p class="lead text-center">Go to the <a href="/">home page</a>.</p>
          </div>
        </article>
        <?php endif; ?>
        <?php get_template_part('includes/contact-block'); ?>
      </div>
    </div>
  </div>
</section>

<?php get_footer(); ?>
