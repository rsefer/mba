jQuery(document).ready(function($) {

  // Hash jacking
  if (window.location.hash && $('body').find(window.location.hash)) {
    setTimeout(function() {
      window.scrollTo(0, 0);
    }, 0);
    $('html, body').animate({
      scrollTop: $('body').find(window.location.hash).offset().top - $('header nav').outerHeight() - 50
    });
  }

});
