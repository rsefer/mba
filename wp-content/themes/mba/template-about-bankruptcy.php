<?php /* Template Name: About Bankruptcy */ ?>

<?php get_header(); ?>

<section id="about-intro" class="hero<?php if (!has_post_thumbnail()) { echo ' about'; } ?>"<?php if (has_post_thumbnail()) { echo ' style="background-image: url(' . wp_get_attachment_url(get_post_thumbnail_id(get_the_ID())) . ');"'; } ?>>
  <div class="container">
    <div class="row">
      <div class="col-sm-12">
        <h1><?php the_title(); ?></h1>
      </div>
    </div>
  </div>
</section>

<section id="about-main">
  <div class="container">
    <div class="row">
      <div class="col-sm-12">
        <?php if(have_posts()) : while(have_posts()) : the_post(); ?>
        <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
          <div class="article-content">
            <?php if (has_nav_menu('about-menu')) { ?>
            <div class="outline about-outline alignleft">
              <div class="outline-title">Learn About Bankruptcy</div>
              <?php
                wp_nav_menu(array(
                  'menu' => 'about-menu',
                  'items_wrap' => '<ol id="%1$s" class="%2$s">%3$s</ol>'
                ));
              ?>
            </div>
            <?php } ?>
            <?php the_content(); ?>
          </div>
        </article>
        <?php endwhile; endif; ?>
        <?php get_template_part('includes/contact-block'); ?>
      </div>
    </div>
  </div>
</section>

<?php get_footer(); ?>
