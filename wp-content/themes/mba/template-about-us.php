<?php /* Template Name: About Us */ ?>

<?php get_header(); ?>

<section id="us-intro" class="hero<?php if (!has_post_thumbnail()) { echo ' pattern'; } ?>"<?php if (has_post_thumbnail()) { echo ' style="background-image: url(' . wp_get_attachment_url(get_post_thumbnail_id(get_the_ID())) . ');"'; } ?>>
  <div class="container">
    <div class="row">
      <div class="col-sm-12">
        <h1><?php the_title(); ?></h1>
      </div>
    </div>
  </div>
</section>

<section id="us-main" class="main">
  <div class="container">
    <div class="row">
      <div class="col-sm-12">
        <?php if(have_posts()) : while(have_posts()) : the_post(); ?>
        <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
          <div class="article-content">
            <?php the_content(); ?>
          </div>
          <?php
            $people_query = new WP_Query(array(
              'post_type' => 'person',
              'posts_per_page' => -1,
              'orderby' => 'menu_order',
              'order' => 'ASC'
            ));
          ?>
          <?php if ($people_query->have_posts()) { ?>
          <div class="about-people-list row">
            <div class="col-sm-12"><h2>Our People</h2></div>
            <?php while($people_query->have_posts()) : $people_query->the_post(); ?>
            <div class="col-md-6 col-sm-12 about-person" itemscope itemtype="http://schema.org/Person">
              <?php
                $person_jobtitle = get_post_meta(get_the_ID(), '_jobtitle', true);
                $person_nickname = get_post_meta(get_the_ID(), '_nickname', true);
                if ($person_nickname) {
                  $working_nickname = $person_nickname;
                } else {
                  $working_nickname = substr(get_the_title(), 0,  strpos(get_the_title(), ' '));
                }
              ?>
              <div class="row">
                <div class="col-sm-5">
                  <?php the_post_thumbnail(); ?>
                </div>
                <div class="col-sm-7">
                  <h4 itemprop="name"><?php the_title(); ?></h4>
                  <?php
                  if ($person_jobtitle) {
                    echo '<h5 itemprop="jobTitle">' . $person_jobtitle . '</h5>';
                  }
                  ?>
                  <p><a href="<?php echo get_the_permalink(get_the_ID()); ?>">Read more about <?php echo $working_nickname; ?></a></p>
                </div>
              </div>
            </div>
            <?php endwhile; ?>
          </div>
          <?php } ?>
        </article>
        <?php endwhile; endif; ?>
        <?php get_template_part('includes/contact-block'); ?>
      </div>
    </div>
  </div>
</section>

<?php get_footer(); ?>
