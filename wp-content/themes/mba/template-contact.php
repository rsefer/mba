<?php /* Template Name: Contact */ ?>

<?php get_header(); ?>

<section id="contact-intro" class="hero relative">
  <div id="contact-map" class="hero-map"></div>
  <div class="container">
    <div class="row">
      <div class="col-sm-12">
        <h1><?php the_title(); ?></h1>
      </div>
    </div>
  </div>
</section>

<section id="contact-main" class="main">
  <div class="container">
    <div class="row">
      <div class="col-sm-12 contact-address-list">
        <h3>Locations</h3>
        <div class="row">
          <?php echo allAddresses(); ?>
        </div>
      </div>
      <div class="col-sm-12">
        <?php if(have_posts()) : while(have_posts()) : the_post(); ?>
        <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
          <?php get_template_part('includes/contact-links'); ?>
          <div class="article-content">
            <?php the_content(); ?>
          </div>
        </article>
        <?php endwhile; endif; ?>
      </div>
      <div class="alt-back alt-back-contact alt-back-contact-alt col-md-6">
        <h3>Contact Us</h3>
        <?php echo do_shortcode('[ninja_forms_display_form id=7]'); ?>
      </div>
      <div class="alt-back alt-back-contact col-md-6">
        <div class="row">
          <div class="col-lg-8 col-lg-offset-2">
            <h3>Free Bankruptcy Evaluation</h3>
            <?php echo do_shortcode('[ninja_forms_display_form id=1]'); ?>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>

<script type='text/javascript' src='https://maps.googleapis.com/maps/api/js?key=AIzaSyBFtFPIjlG_bdt5h9K6pWEPJZYYpE11LEw&#038;ver=4.2.2'></script>
<script>
function initialize() {
  var styleStark = [
    {
      'featureType': 'administrative.neighborhood',
      'stylers': [
      { 'visibility': 'off' }
      ]
    },{
      'featureType': 'landscape.man_made',
      'stylers': [
      { 'visibility': 'off' }
      ]
    },{
      'featureType': 'poi',
      'stylers': [
      { 'visibility': 'off' }
      ]
    },{
      'featureType': 'road.highway',
      'stylers': [
      { 'saturation': -100 },
      { 'visibility': 'simplified' }
      ]
    },{
      'featureType': 'road.arterial',
      'elementType': 'labels',
      'stylers': [
      { 'visibility': 'off' }
      ]
    },{
      'featureType': 'road.local',
      'stylers': [
      { 'visibility': 'off' }
      ]
    },{
      'featureType': 'transit',
      'stylers': [
      { 'visibility': 'off' }
      ]
    },{
      'featureType': 'landscape',
      'elementType': 'geometry.fill',
      'stylers': [
      { 'color': '#b3b3b3' }
      ]
    }, {
      'featureType': 'landscape',
      'stylers': [
      {
        'saturation': -100
      },
      {
        'lightness': 65
      },
      {
        'visibility': 'on'
      }
      ]
    }, {
      'featureType': 'water',
      'elementType': 'geometry',
      'stylers': [
      { 'hue': '#ffff00' },
      { 'lightness': -25 },
      { 'saturation': -97 }
      ]
    }
  ];
  var officeLat = 41.888;
  var officeLng = -87.630;
  var horizontalOffset;
  if (window.innerWidth < 768) {
    horizontalOffset = 0.010;
  } else {
    horizontalOffset = 0.040;
  }
  var offsetLocation = new google.maps.LatLng(officeLat + 0.002, officeLng - horizontalOffset);
  var officeLocation = new google.maps.LatLng(officeLat, officeLng);
  var mapOptions = {
    center: offsetLocation,
    zoom: 13,
    disableDefaultUI: true,
    disableDoubleClickZoom: true,
    draggable: false,
    keyboardShortcuts: false,
    scrollwheel: false,
    styles: styleStark
  };
  var map = new google.maps.Map(document.getElementById('contact-map'), mapOptions);
  var marker = new google.maps.Marker({
    position: officeLocation,
    map: map
  });
}

google.maps.event.addDomListener(window, 'load', initialize);
</script>

<?php get_footer(); ?>
