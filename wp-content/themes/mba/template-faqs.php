<?php /* Template Name: FAQs */ ?>

<?php get_header(); ?>

<section id="faqs-intro" class="hero">
  <div class="container">
    <div class="row">
      <div class="col-sm-12">
        <h1><?php the_title(); ?></h1>
      </div>
    </div>
  </div>
</section>

<section id="faqs-main" class="main">
  <div class="container">
    <div class="col-sm-12">
      <?php if(have_posts()) : while(have_posts()) : the_post(); ?>
      <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
        <div class="article-content">
          <?php the_content(); ?>
          <?php
            $faqsList = get_field('question_and_answer');
            if ($faqsList) {
              echo '<ul class="faqs-list">';
              $i = 1;
              foreach ($faqsList as $qa) {
                echo '<li id="faq-' . $i . '" class="qa">';
                  echo '<div class="question">' . $qa['question'] . '</div>';
                  echo '<div class="answer">' . $qa['answer'] . '</div>';
                echo '</li>';
                $i++;
              }
              echo '</ul>';
            }
          ?>
        </div>
      </article>
      <?php endwhile; endif; ?>
      <?php get_template_part('includes/contact-block'); ?>
    </div>
  </div>
</section>

<?php get_footer(); ?>
