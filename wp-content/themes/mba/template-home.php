<?php /* Template Name: Home */ ?>

<?php get_header(); ?>

<section id="home-intro" class="hero pattern">
  <div class="container">
    <div class="row">
      <div class="col-md-6">
        <h1><span class="hidden-xs">We Are Your<?php echo bsNewLine(); ?>Chicagoland<?php echo bsNewLine(); ?>Bankruptcy Attorneys</span><span class="hidden-sm hidden-md hidden-lg">We Are Your Chicagoland Bankruptcy Attorneys</span></h1>
        <div class="row home-buttons-wrap">
          <a href="/free-e-book-sign-up/" class="col-xs-12 col-md-10 col-lg-9 btn-download-ebook btn btn-lg btn-warning">Click Here to Download<br>Our <span class="free-highlight">Free E-Book</span> Now</span></a>
          <a href="/testimonials/" class="col-xs-12 col-md-10 col-lg-9 btn-testimonials btn btn-lg btn-primary">What Our Clients<br>Say About Us</a>
        </div>
      </div>
      <div class="col-md-6">
        <ul class="home-list">
          <li>Stop Wage Garnishment</li>
          <li>Eliminate Credit Card Debt</li>
          <li>Wipe Out Medical Bills</li>
          <li>Get Your License Back</li>
          <li>End Harassing Calls</li>
          <li>Rebuild Your Credit</li>
        </ul>
      </div>
    </div>
  </div>
</section>

<section id="home-main">
  <div class="container">
    <div class="row">
      <div class="col-sm-12">
        <div class="location-bar"><div class="location-title intro">Locations</div><a  class="location-title" href="https://www.google.com/maps/place/321+N+Clark+Street%2C+Chicago%2C+IL+60654/">Chicago Loop</a><a class="location-title" href="https://www.google.com/maps/place/6200+N+Hiawatha+Ave,+Chicago,+IL+60646/">Chicago Northside</a><a  class="location-title" href="https://www.google.com/maps/place/10540+S+Western+Ave,+Chicago,+IL+60643/">Chicago Southside</a><a class="location-title" href="https://www.google.com/maps/place/601+Skokie+Blvd,+Northbrook,+IL+60062/">Northbrook</a></div>
      </div>
      <div class="col-md-6 col-md-push-6">
        <?php if(have_posts()) : while(have_posts()) : the_post(); ?>
          <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
            <div class="article-content">
              <a href="<?php echo get_the_permalink(12); ?>"><img class="aligncenter content-image" src="/images/graphics/Eliminate_Debt.jpg" alt="Eliminate Debt" /></a>
              <?php the_content(); ?>
            </div>
            <?php get_template_part('includes/contact-links'); ?>
            <img class="bbb-a-plus" src="/images/bbb-a.png">
          </article>
        <?php endwhile; endif; ?>
      </div>
      <div class="alt-back alt-back-contact col-md-5 col-md-pull-6">
        <div class="row">
          <div class="col-lg-8 col-lg-offset-2">
            <h3>Free Bankruptcy Evaluation</h3>
            <?php echo do_shortcode('[ninja_forms_display_form id=1]'); ?>
          </div>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="alt-back payment-block-wrap col-md-5">
        <form action="https://checkout.payhub.com/transaction/paynow" id="pSDSkTc_-rpgQhM9MFEIbA" method="post" target="_payhubcart" class="payment-block">
          <h4>Make A Payment</h4>
          <input type="hidden" name="mode" value="live" />
          <input type="hidden" name="orgid" value="15888" />
          <!--<label for="ph-invoice-num">Invoice # </label>
          <input type="text" name="invoice" id="ph-invoice-num" placeholder="Invoice #" pattern="^[0-9a-zA-Z_ -]{1,20}" /><br />-->
          <div class="row">
            <div class="col-md-6">
              <label for="ph-payment-amount">Payment Amount</label>
              <input type="text" name="amount" id="ph-payment-amount" placeholder="$999.99" required="true" pattern="^[0-9.]{1,12}$" class="form-control" />
            </div>
            <div class="col-md-6">
              <label>&nbsp;</label>
              <input type="submit" class="btn btn-block btn-success" value="Submit" onClick="document.getElementById("pSDSkTc_-rpgQhM9MFEIbA").submit(); return false;">
              <input type="hidden" name="custom_email_message" value="" />
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
</section>

<?php get_footer(); ?>
