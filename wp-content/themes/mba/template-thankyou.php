<?php /* Template Name: Thank You */ ?>

<?php get_header(); ?>

<section id="thankyou-intro" class="hero<?php if (!has_post_thumbnail()) { echo ' pattern'; } ?>"<?php if (has_post_thumbnail()) { echo ' style="background-image: url(' . wp_get_attachment_url(get_post_thumbnail_id(get_the_ID())) . ');"'; } ?>>
  <div class="container">
    <div class="row">
      <div class="col-sm-12">
        <h1><?php the_title(); ?></h1>
      </div>
    </div>
  </div>
</section>

<section id="thankyou-main" class="main">
  <div class="container">
    <div class="row">
      <div class="col-sm-12">
        <?php if(have_posts()) : while(have_posts()) : the_post(); ?>
        <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
          <div class="article-content">
            <?php the_content(); ?>
          </div>
        </article>
        <?php endwhile; else : ?>
        <article>
          <h1 class="title">Not Found</h1>
          <div class="article-content">
            <p class="lead text-center">Go to the <a href="/">home page</a>.</p>
          </div>
        </article>
        <?php endif; ?>
        <?php get_template_part('includes/contact-block'); ?>
      </div>
    </div>
  </div>
</section>

<!-- Google Code for Contact Form Conversion Page -->
<script type="text/javascript">
/* <![CDATA[ */
var google_conversion_id = 955209468;
var google_conversion_language = "en";
var google_conversion_format = "2";
var google_conversion_color = "ffffff";
var google_conversion_label = "-hlgCOOGnFkQ_K29xwM";
var google_remarketing_only = false;
/* ]]> */
</script>
<script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
</script>
<noscript>
<div style="display:inline;">
<img height="1" width="1" style="border-style:none;" alt="" src="//www.googleadservices.com/pagead/conversion/955209468/?label=-hlgCOOGnFkQ_K29xwM&amp;guid=ON&amp;script=0"/>
</div>
</noscript>

<?php get_footer(); ?>
